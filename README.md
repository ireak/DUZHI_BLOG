#DUZHI_BLOG
看来还是得写完后写个总结，增加内容更新贴http://www.duzhi.me/duzhi_up_log.html
有兴趣可以访问Seesee ,当然从今天开始更新的哦，如果想找历史的日志之类的，提供了标签URL:
http://www.duzhi.me/article-jpress-1.html
大家也可以查看查看
群支持：[![image](http://pub.idqqimg.com/wpa/images/group.png)](http://shang.qq.com/wpa/qunwpa?idkey=91cca8513df050005503fbf2bb899d339593d19f01917914bceee8ae308c5b6b)
### 版本
DUZHI_BLOG基于Jpress(http://jpress.io)进行更新而来的
Jpress 基本采用了Jfinal 的MVC 以及Module 的快速开发模式。“模型及所得”的开发模式

### 组件
#### Jfinal
这个就不说了，一个MVC框架。
#### Ehcache 
缓存框架，因为这个里面考虑的是本地的缓存所以，单机模式，所以在缓存的方案基本是懒加载以及缓存加载模式
~~~java
	public User getUser() {
		if (user != null)
			return user;

		if (getUserId() == null)
			return null;

		user = UserQuery.me().findById(getUserId());
		return user;
	}
~~~
上面是个Content 模型中获取用户的模式。
再举个缓存的例子；
~~~java
	if (ActionCacheManager.isCloseActionCache()) {
			next.handle(target, request, response, isHandled);
			return;
		}

		Action action = JFinal.me().getAction(target, urlPara);
		if (action == null) {
			next.handle(target, request, response, isHandled);
			return;
		}

		ActionCache actionCache = action.getMethod().getAnnotation(ActionCache.class);
		if (actionCache == null) {
			actionCache = action.getControllerClass().getAnnotation(ActionCache.class);
			if (actionCache == null) {
				next.handle(target, request, response, isHandled);
				return;
			}
		}

		String originalTarget = (String) request.getAttribute("_original_target");
		String cacheKey = StringUtils.isNotBlank(originalTarget) ? originalTarget : target;

		String queryString = request.getQueryString();
		if (queryString != null) {
			queryString = "?" + queryString;
			cacheKey += queryString;
		}

		ActionCacheManager.enableCache(request);
		ActionCacheManager.setCacheKey(request, cacheKey);
		ActionCacheManager.setCacheContentType(request, actionCache.contentType());

		String renderContent = ActionCacheManager.getCache(request, cacheKey);
		if (renderContent != null) {
			response.setContentType(actionCache.contentType());

			PrintWriter writer = null;
			try {
				writer = response.getWriter();
				writer.write(renderContent);
				isHandled[0] = true;
			} catch (Exception e) {
				RenderFactory.me().getErrorRender(500).setContext(request, response, action.getViewPath()).render();
			} finally {
				if (writer != null) {
					writer.close();
				}
			}
		} else {
			next.handle(target, request, response, isHandled);
		}
~~~

上面是ActionCacheHandler.java的代码。


### db.properties
由于Jpress 使用初始化模式，判断的就是db.properties 有没有，所以如果没有数据库，已经表结构的情况下，建议使用懒汉模式一步一步安装即可。
~~~java
	rm db.properties

~~~
### 使用指南：
Jpress 相关文章：http://www.duzhi.me/article-jpress-1.html
DUZHI_BLOG主题使用配置指南：http://www.duzhi.me/article/1105.html
DUZHI_BLOG 分支开发使用指南：http://www.duzhi.me/article/1104.html
更新Log:http://www.duzhi.me/duzhi_up_log.html
Jpress 二次开发手册：http://www.duzhi.me/jpress_doc.html

先写这么多，后面慢慢补充

当前网站：http://www.duzhi.me