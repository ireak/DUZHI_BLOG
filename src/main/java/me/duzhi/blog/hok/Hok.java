package me.duzhi.blog.hok;

import java.lang.annotation.*;

/**
 * @author ashang.peng@aliyun.com
 * @date 二月 14, 2017
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Hok {

    public static final int DEFAULT_WEIGHT = 10;
    /**
     * 操作
     */
    String[] action();

    int weight() default DEFAULT_WEIGHT;
}
