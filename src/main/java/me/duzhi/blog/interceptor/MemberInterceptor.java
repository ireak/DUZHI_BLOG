package me.duzhi.blog.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import io.jpress.interceptor.InterUtils;
import io.jpress.model.User;

/**
 * @author ashang.peng@aliyun.com
 * @date 一月 03, 2017
 */

public class MemberInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation inv) {
        User user = InterUtils.tryToGetUser(inv);
        Controller controller = inv.getController();
        String target = controller.getRequest().getRequestURI();
        String cpath = controller.getRequest().getContextPath();
        if (!target.startsWith(cpath + "/admin")) {
            if (user != null) {
                controller.setAttr("_USER", user);
            }
        }
        inv.invoke();
    }
}
